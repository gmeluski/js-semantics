

const zeroMatrix = (matrix) => {
  // identify the 0s and their positions
  if (!matrix || matrix.length === 0) {
    throw new Error('no');
  }

  const matrixHeight = matrix.length;
  const elementsPerRow = matrix[0].length;

  // tweleve elements
  // lets flatten to keep from nesting loops
  const flatMatrix = [].concat.apply([], matrix);
  const zeroIndicies = flatMatrix.map((element, index) => element === 0 ? index : '').filter(String);

  // derive the row and column from the flattened Index
  const zeroLocations = zeroIndicies.map(zeroIndex => {
    const zeroRow = Math.ceil((zeroIndex + 1) / elementsPerRow) - 1;  //zeroIndicies[0]
    const zeroColumn = zeroIndex - elementsPerRow * zeroRow;
    return [zeroRow, zeroColumn];
  })

  zeroLocations.forEach((zeroLocation) => {
    const [rowToZero, columnToZero] = zeroLocation;
    const startpoint = rowToZero * elementsPerRow;
    const endpoint = startpoint + elementsPerRow;

    // zero the row
    for (let i = startpoint; i < endpoint; i++) {
      flatMatrix[i] = 0;
    }

    // zero the column
    for (let i = 0; i < matrixHeight; i++) {
      const firstIndexInRow = i * elementsPerRow;
      const columnInRow = firstIndexInRow + columnToZero;
      flatMatrix[columnInRow] = 0;
    }
  });

  // reassemble here
  const assembled = [];
  while (flatMatrix.length) {
    assembled.push(flatMatrix.splice(0, elementsPerRow))
  }

  return assembled;
}


export default zeroMatrix;
