import stringRotation from '../stringRotation';


describe('is this a rotation', () => {
  it('works', () => {
    const originalString = 'erbottlewat'
    const changedString = 'waterbottle';

    expect(stringRotation(originalString, changedString)).toEqual(true);
  });

  it('fails if theres at least one character off', () => {
    const originalString = 'erbottlewat'
    const changedString = 'waterbottlf';

    expect(stringRotation(originalString, changedString)).toEqual(false);
  });
});
