import zeroMatrix from '../zeroMatrix';

describe('zeroing the matrix', () => {
  it('zeros with a single instance', () => {
    const simpleStuffAgain = [
      [1, 1, 2, 3],
      [3, 4, 0, 6],
      [6, 7, 8, 9],
    ];

    const simpleStuffAgainFix = [
      [1, 1, 0, 3],
      [0, 0, 0, 0],
      [6, 7, 0, 9],
    ];

    expect(zeroMatrix(simpleStuffAgain)).toEqual(simpleStuffAgainFix);
  });

  it('zeros with a multiple instances', () => {
    const multiples = [
      [0, 1, 2],
      [3, 0, 5],
      [6, 7, 8],
    ]

    const multiplesFixed = [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 8],
    ]

    expect(zeroMatrix(multiples)).toEqual(multiplesFixed);
  });
});
