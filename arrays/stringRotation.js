const stringRotation = (original, changed) => {
  const longBoy = original + original;
  return Boolean(longBoy.indexOf(changed) > -1);
};

export default stringRotation;
