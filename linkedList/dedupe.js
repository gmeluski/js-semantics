const dedupe = (list) => {
  let foundInfo = getDupes(list.head);
  const values = Object.values(foundInfo)
  const indicesToRemove = values.reduce((accumulator, value) => [...accumulator, ...value], []);

  indicesToRemove.forEach((index) => {
    list.deleteNodeByIndex(index);
  });

}

const getDupes = (head) => {
  let currentNode = head;
  let foundInfo = {};
  let index = 0;

  while (currentNode !== null) {
    const savedPosition = foundInfo[currentNode.info];

    if (savedPosition) {
      foundInfo[currentNode.info].push(index);
    } else {
      foundInfo[currentNode.info] = [];
    }

    currentNode = currentNode.next;
    index++;
  }

  return foundInfo;
}

export default dedupe;
