class Node {
  constructor(info) {
    this.info = info;
    this.next = null;
  }

  appendToTail(data) {
    const end = new Node(data)
    let currentNode = this;

    while (currentNode.next !== null) {
      currentNode = currentNode.next;
    }

    currentNode.next = end;
  }
}

export default Node;
