import Node from './Node';

class LinkedList {
  getNode(index) {
    let i = 0;
    let currentNode;
    while (i <= index && currentNode !== null) {
      if (!currentNode) {
        currentNode = this.head;
      } else {
        currentNode = currentNode.next;
      }
      i++;
    }

    return currentNode;
  }

  createFromList(initialData) {
    let primaryNode;
    let currentNode;
    let currentData;
    let data = initialData.slice();

    while (data.length > 0) {
      currentData = data.shift();
      if (!currentNode) {
        currentNode = new Node(currentData);
        primaryNode = currentNode;
      } else {
        currentNode.appendToTail(currentData);
        currentNode = currentNode.next;
      }
    }

    this.setHead(primaryNode);
    return primaryNode;
  }

  countNodesByInfo(info) {
    let count = 0;
    let currentNode = this.head;

    while (currentNode !== null) {
      if (currentNode.info === info) {
        count++;
      }

      currentNode = currentNode.next;
    }

    return count;
  }

  setHead(node) {
    this.head = node;
  }

  deleteNodeByIndex(indexToDelete) {
    let currentNode = this.head
    let index = 0;
    while (currentNode.next !== null) {
      // this needs to look back at the previous node and assign next
      if ((index + 1) === indexToDelete) {
        currentNode.next = currentNode.next.next;
        return;
      }

      currentNode = currentNode.next;
      index++;
    }
    return;
  }

  deleteNode(matchInfo) {
    let currentNode = this.head

    if (currentNode.info === matchInfo) {
      this.setHead(currentNode.next);
      return;
    }

    while (currentNode.next !== null) {
      if (currentNode.next.info === matchInfo) {
        currentNode.next = currentNode.next.next;
        return;
      }

      currentNode = currentNode.next;
    }

    return;
  }
}

export default LinkedList;
