const kth = (head, k) => {
  // we dont know how far we are from the tail!
  // is it worth hashing them?

  let currentNode = head;

  for (var i = 0; i < k; i++) {
    if (currentNode === null) {
      return null;
    }

    currentNode = currentNode.next;
  }

  while (currentNode !== null) {
    head = head.next;
    currentNode = currentNode.next;
  }

  return head;
};

export default kth;
