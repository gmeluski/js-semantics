import LinkedList from '../LinkedList';
import kth from '../kth';

// I would consider 'g' to be second from last, but...
// do different individuals define kth from last differently?
// eg do people consider the 'g' here to be 2nd or 1st from last
describe('getting the kth from the last in the list', () => {
  const data = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

  it('works', () => {
    const linkedList = new LinkedList();
    linkedList.createFromList(data);
    const positionFromLast = 3;

    const indexToFind = data.length - positionFromLast;
    expect(kth(linkedList.head, positionFromLast).info).toEqual(data[indexToFind])
  });
});
