import LinkedList from '../LinkedList';

describe('linked list functionality', () => {
  const data = ['a', 'b', 'c', 'd'];
  it('adds multiple nodes successfully', () => {
    const linkedList = new LinkedList();
    const primaryNode = linkedList.createFromList(data);

    let testNode = primaryNode;
    let listLength = 0;
    while (testNode !== null) {
      listLength++;
      testNode = testNode.next;
    }
    expect(listLength).toEqual(4);
  });

  it('retrieves a node when given an index', () => {
    const linkedList = new LinkedList();
    linkedList.createFromList(data);

    const toFind = 2;
    const foundNode = linkedList.getNode(toFind)

    expect(foundNode.info).toEqual(data[toFind]);

  });

  it('deletes a node as expected', () => {
    const linkedList = new LinkedList();
    linkedList.createFromList(data);
    const toDelete = 2;
    linkedList.deleteNode(data[toDelete]);

    expect(linkedList.getNode(toDelete).info).toEqual(data[toDelete + 1]);
  });

  it('deletes a node by index', () => {
    const linkedList = new LinkedList();
    linkedList.createFromList(data);
    const toDelete = 2;
    const replacement = linkedList.getNode(toDelete).next;

    linkedList.deleteNodeByIndex(toDelete);
    expect(linkedList.getNode(toDelete)).toEqual(replacement);
  });

  it('deletes the last node', () => {
    const linkedList = new LinkedList();
    linkedList.createFromList(data);
    const toDelete = data.length - 1;
    linkedList.deleteNode(data[toDelete])
    expect(linkedList.getNode(toDelete)).toEqual(null);
  });

  it('counts the number of instances of certain info', () => {
    const linkedList = new LinkedList();
    linkedList.createFromList(['a', 'b', 'c', 'b', 'd', 'e']);
    const count = linkedList.countNodesByInfo('b');
    expect(count).toEqual(2);

  })
});
