import LinkedList from '../LinkedList';
import dedupe from '../dedupe';

describe('deduping the list', () => {
  let primaryNode;

  it('removes the offender', () => {
    const data = ['a', 'b', 'a', 'c'];
    const linkedList = new LinkedList();
    linkedList.createFromList(data);
    dedupe(linkedList);
    const count = linkedList.countNodesByInfo('a');
    expect(count).toEqual(1);
  });
});
