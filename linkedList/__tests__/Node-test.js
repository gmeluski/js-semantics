import Node from '../Node';

describe('node functionality', () => {
  it('adds a new node successfully', () => {
    const primaryNode = new Node('me');
    primaryNode.appendToTail('you');

    expect(primaryNode.next.info).toEqual('you');
  })
});
