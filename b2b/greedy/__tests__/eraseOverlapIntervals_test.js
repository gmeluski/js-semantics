import eraseOverlapIntervals from '../eraseOverlapIntervals';

describe('erase overlap intervals', () => {
  it('works with no overlaps', () => {
    const input = [
      [1, 2],
      [2, 3],
      [3, 4],
    ];
    const output = 0;

    expect(eraseOverlapIntervals(input)).toEqual(output);
  })

  it('works with an overlap', () => {
    const input = [
      [1, 2],
      [1, 2],
    ];
    const output = 1;

    expect(eraseOverlapIntervals(input)).toEqual(output);
  })
})
