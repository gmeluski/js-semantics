const sortByEnd = (intervalA, intervalB) => {
  return (intervalA[1] < intervalB[1]) ? -1 : 1;
};

const eraseOverlapIntervals = (intervals) => {
  intervals.sort(sortByEnd);
  let activeEndpoint = intervals[0][1];
  let maximalSetCount = 1;

  for (var i = 1; i < intervals.length; i++) {
    const currentPair = intervals[i];
    const start = currentPair[0];
    const end = currentPair[1];

    // if no overlap between current
    // interval start & the active end,
    // make the next interval active
    if (start >= activeEndpoint) {
      activeEndpoint = end;
      maximalSetCount++;
    }
  }

  return intervals.length - maximalSetCount;
}

export default eraseOverlapIntervals;
