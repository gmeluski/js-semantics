const zigzag = (string, rows) => {
  if (rows === 1) {
    return string;
  }

  let stringPointer = 0;
  let rowPointer = 0;
  let operation;
  const matrix = [...Array(rows)].map(() => []);

  while (stringPointer < string.length) {
    matrix[rowPointer].push(string[stringPointer]);

    if (rowPointer === 0) {
      operation = 'increment';
    } else if (rowPointer === rows - 1) {
      operation = 'decrement';
    }

    if (operation === 'increment') {
      rowPointer++;
    } else {
      rowPointer--;
    }

    stringPointer++;
  }

  return matrix.reduce((accumulator, value) => accumulator.concat(value), []).join('');
};

export default zigzag;
