const findAndReplacePattern = (words, pattern) => {
  const patternSequence = getSequence(pattern);
  const wordsAsSequences = words.map(getSequence);

  return wordsAsSequences.reduce((accumulator, sequence, position) => {
    if (JSON.stringify(sequence)==JSON.stringify(patternSequence)) {
      accumulator.push(words[position]);
    }
    return accumulator;
  }, []);
}

const getSequence = (word) => {
  let patternMap = {};
  let marker = 0;
  let sequence = [];

  for (let i = 0; i < word.length; i++) {
    const currentCharacter = word[i];
    if (patternMap[currentCharacter] !== undefined) {
      sequence.push(patternMap[currentCharacter]);
    } else {
      sequence.push(marker);
      patternMap[currentCharacter] = marker;
      marker++;
    }
  }

  return sequence;
}


const findAndReplaceCharacterPattern = (words, pattern) => {
  // what if pattern is length 0 or 1
  const positions = mapPatternToPositions(pattern);
  const wordsToPosition = words.map((word) => mapPatternToPositions(word));

  return wordsToPosition.reduce((accumulator, wordPositions, position) => {
    if (JSON.stringify(wordPositions)==JSON.stringify(positions)) {
      accumulator.push(words[position]);
    }

    return accumulator;
  }, []) ;

}

const mapPatternToPositions = (pattern) => {
  let marker;
  let positions = [];
  for (let i = 0; i < pattern.length; i++) {
    const currentCharacterCode = pattern[i].charCodeAt();

    let relativePosition;
    if (i === 0) {
      marker = currentCharacterCode;
      relativePosition = 0;
    } else {
      const characterDistance = currentCharacterCode - marker;
      relativePosition = (characterDistance >= 0) ? characterDistance : -1;
    }

    positions.push(relativePosition)
  }

  return positions;
}

export default findAndReplacePattern;
