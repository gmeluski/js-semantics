const longestPalindrome = (string) => {
  const unmatched = new Set();
  let matchCount = 0;
  let asArray = string.split('');

  while (asArray.length > 0) {
    const character = asArray.shift();
    if (unmatched.has(character)) {
      matchCount++;
      unmatched.delete(character);
    } else {
      unmatched.add(character);
    }
  }

  const unmatchedBump = unmatched.size > 0 ? 1 : 0;
  return (matchCount * 2) + unmatchedBump;

};


export default longestPalindrome;
