import patternMatching from '../patternMatching';

describe('pattern matching', () => {
  it('matches on the same letter pattern', () => {
    expect(patternMatching(["aa", "bb"], 'cc')).toEqual(['aa', 'bb']);
  });

  it('matches on a varied letter patern', () => {
    const words = ["aac", "bbc", "bcb", "yzy"]
    const pattern = "ghg"
    const result = ["bcb", "yzy"]
    expect(patternMatching(words, pattern)).toEqual(result);
  });

  it('fails on a reverse', () => {
    const words = ["xx","yy","ka"]
    const pattern = "ba";
    expect(patternMatching(words, pattern)).toEqual(['ka']);
  })
  it('fails on a bad match', () => {
    const words = ["aa", "bb"]
    const pattern = "zy"
    expect(patternMatching(words, pattern)).toEqual([]);

  });
});
