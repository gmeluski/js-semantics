import longestPalindrome from '../longestPalindrome';

describe('all the things', () => {
  it('works with a simple example', () => {
    const input = "aabbc";
    expect(longestPalindrome(input)).toEqual(5);
  });

  it('works with a more complex example', () => {
    const input = "abbcccd"
    expect(longestPalindrome(input)).toEqual(5);
  })

  it('works with a more complex example', () => {
    const input = "aA"
    expect(longestPalindrome(input)).toEqual(1);
  })

  it('works with a more complex example', () => {
    const input = "xyz"
    expect(longestPalindrome(input)).toEqual(1);
  })

  it('works with a more complex example', () => {
    const input = "ccc"
    expect(longestPalindrome(input)).toEqual(3);
  })
});
