import zigzag from '../zigzag';

describe('the zigzag function', () => {
  it('works with more rows than two', () => {
    const string = "YELLOWPINK";
    const rows = 4;

    expect(zigzag(string, rows)).toEqual("YPEWILONLK")
  });

  it('works with two rows', () => {
    const string = "REDBLUEBLACK"
    const rows = 2
    expect(zigzag(string, rows)).toEqual("RDLELCEBUBAK")
  })

  it('works with one row', () => {
    const string = "REDBLUEBLACK"
    const rows = 1

    expect(zigzag(string, rows)).toEqual("REDBLUEBLACK")
  })
})
