import minWindow from '../minWindow.js'
import optimized from '../minWindowOptimized.js'

describe('find the sub stringggg', () => {
  it('works with brute force', () => {
    const myString = "donutsandwafflemakemehungry";
    const target = 'flea';
    const result = minWindow(myString, target);

    //"affle" or "flema"
    expect(result).toEqual('affle');
  });

  it('works with optimized', () => {
    const myString = "whoopiepiesmakemyscalegroan";
    const target = 'roam';
    const result = optimized(myString, target);

    //"affle" or "flema"
    expect(result).toEqual('myscalegro');
  });
});
