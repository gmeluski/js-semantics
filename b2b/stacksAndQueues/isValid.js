const isValid = (string) => {
  if (string.length % 2 !== 0) {
    return false;
  }

  const found = [];
  const openers = ['(', '[', '{'];


  let i = 0;
  let isBalanced = true;
  while (i < string.length && isBalanced) {
    const character = string[i];
    if (openers.includes(character)) {
      found.push(character);
    } else {
      if (found.length === 0) {
        isBalanced = false;
      } else {
        const testForOpen = found.pop();
        isBalanced = openerMatches(testForOpen, character)
      }
    }

    i++;
  }

  return isBalanced && found.length === 0;

}

const openerMatches = (potentialOpener, closingCharacter) => {
  switch (closingCharacter) {
    case ')':
      return potentialOpener === '(';
    case ']':
      return potentialOpener === '[';
    case '}':
      return potentialOpener === '{';
    default:
      return false;
  }

}

export default isValid;
