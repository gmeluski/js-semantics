import isValid from '../isValid'

describe('isValid', () => {
  it('fails on imbalance', () => {
    const tester = ']';
    expect(isValid(tester)).toEqual(false);
  });


  it('works for a simple case', () => {
    const tester = '()';

    expect(isValid(tester)).toEqual(true);

  });

  it('fails on a simple balanced', () => {
    const tester = '][';
    expect(isValid(tester)).toEqual(false);
  })

  it('fails when lacking the right amount of openers', () => {
    const tester = '()))';
    expect(isValid(tester)).toEqual(false);

  });

  it('fails on a balanced with extra braces', () => {
    const tester = '((([[)))';
    expect(isValid(tester)).toEqual(false);
  })

  it('super complicated stuff', () => {
    const tester = '{{{{{{{{{{}}}}}}}}}}[[[[[[[[[[]]]]]]]]]](((((((((()))))))))){{{[][][](([][][]))}}}{{{{{{{{{{{{{{{{{{{{{{(((())))[][][]}}}}()()()({}{}{})()()()([][][])()()()()()()()()()()()()()()(({}{}{}{}{}{}{}{}))[][][[[]]]';

    expect(isValid(tester)).toEqual(false);
  });
})
