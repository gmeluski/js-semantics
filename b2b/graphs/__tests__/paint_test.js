import paint from '../paint';

describe('breadth first painting', () => {
  it('works', () => {
    const image = [
      [0, 0, 1, 0],
      [0, 0, 1, 0],
      [0, 0, 1, 0],
      [0, 0, 1, 0]
    ]
    const row = 0
    const col = 1
    const newColor = 1

    const output = [
      [1, 1, 1, 0],
      [1, 1, 1, 0],
      [1, 1, 1, 0],
      [1, 1, 1, 0],
    ]

    expect(paint(image, row, col, newColor)).toEqual(output);

  })
})
