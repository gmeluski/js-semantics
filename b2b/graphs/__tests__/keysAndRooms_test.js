import keysAndRooms from '../keysAndRooms';

describe('unlocking it all', () => {
  it('works on the basic case', () => {
    const rooms = [[1], [2], [3], []];
    expect(keysAndRooms(rooms)).toEqual(true);
  });

  it('fails on a basic case', () => {
    const rooms = [[], [2], [3], []]
    expect(keysAndRooms(rooms)).toEqual(false);
  });
});
