/**
 * @param {Array<Array<number>>} rooms
 * @return {boolean}
 */

const localShift = (q) => {
  return [q[0], q.slice(1)]
}

const openRoom = (isOpen, roomNumber) => {
  return {...isOpen, ...{[`${roomNumber}`]: true}};
}

const canVisitAllRooms = (rooms) => {
  let isOpen = {};
  for (let i = 0; i < rooms.length; i++) {
    isOpen[i] = false;
  }

  let keyList = [0];

  while (keyList.length > 0) {
    const [currentKey, updatedList] = localShift(keyList);
    isOpen = openRoom(isOpen, currentKey);
    const foundKeys = rooms[currentKey];
    keyList = [...updatedList, ...foundKeys];
  }
  return Object.values(isOpen).every((value) => value === true);
}

export default canVisitAllRooms;
