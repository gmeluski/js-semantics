const getAdjacents = (row, col, max) => {
  let toMerge = [];
  // North
  if (row > 0) {
    toMerge.push([row - 1, col])
  }

  // South
  if (row < max) {
    toMerge.push([row + 1, col]);
  }

  // West
  if (col > 0) {
    toMerge.push([row, col - 1])
  }

  // East
  if (row < max) {
    toMerge.push([row, col + 1]);
  }

  return toMerge;
}

const paint = (image, startRow, startColumn, newColor) => {
    let paintQueue = [];
    paintQueue.push([startRow, startColumn])
    let i = 0;
    while (paintQueue.length > 0) {
      const [currentRow, currentColumn] = paintQueue.shift();
      const currentColor = image[currentRow][currentColumn];
      if (currentColor !== newColor) {
        const toQueue = getAdjacents(currentRow, currentColumn, image.length - 1)
        paintQueue = [].concat(paintQueue, toQueue);
        image[currentRow][currentColumn] = newColor;
      }
    }
    return image;
}

export default paint
