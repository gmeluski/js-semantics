const minWindowOptimized = (searchString, target) => {

  let leftPointer = 0;
  let rightPointer = 0;
  let currentWindow;
  let hasAllCharacters = false;
  let bestString = '';
  let bestLength = Number.MAX_VALUE;


  while (rightPointer < searchString.length) {
    if (hasAllCharacters === false) {
      rightPointer++;
    } else {
      leftPointer++;
    }

    currentWindow = searchString.substring(leftPointer, rightPointer)
    hasAllCharacters = containsAllCharacters(currentWindow, target);

    if (hasAllCharacters && currentWindow.length < bestLength) {
      bestString = currentWindow;
      bestLength = currentWindow.length;
    }
  }

  return bestString;
};


const createStorage = (target) => {
  const storage = new Map();

  for (var i = 0; i < target.length; i++) {
    if (storage.has(target[i])) {
      let count = storage.get(target[i]);
      storage.set(target[i], count + 1);
    } else {
      storage.set(target[i], 1);
    }
  }

  return storage;
}

const containsAllCharacters = (searchString, target) => {
  const storage = createStorage(target);

  for (var i = 0; i < searchString.length; i++) {
    var currentCharacter = searchString[i];
    if (storage.has(currentCharacter)) {
      const currentCount = storage.get(currentCharacter)

      if (currentCount === 1) {
        storage.delete(currentCharacter);
      } else {
        storage.set(currentCharacter, currentCount - 1);
      }
    }
  }

  return storage.size === 0;
}

export default minWindowOptimized;
