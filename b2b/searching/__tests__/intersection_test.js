import intersection from '../intersection';

describe('search intersection', () => {
  it('handles a straighforward case', () => {
    const listOne = [1,2,3,5];
    const listTwo = [1,2];
    const result = [1,2];
    expect(intersection(listOne, listTwo)).toEqual(result);
  });

  it('handles a case with dupes', () => {
    const listOne = [1,2,2,3]
    const listTwo = [1,1,4]
    const result = [1];

    expect(intersection(listOne, listTwo)).toEqual(result);
  });

  it('handles long dupes', () => {
    const listOne = [0,0,0,0,0,1,1,1,1,2,4];
    const listTwo = [0,0,0,0,1,1,2,2,2];
    const result = [0, 1, 2];

    expect(intersection(listOne, listTwo)).toEqual(result);
  })
});
