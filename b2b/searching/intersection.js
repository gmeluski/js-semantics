const intersection = (nums1, nums2) => {

  let indexOne = 0;
  let indexTwo = 0;
  let valueOne;
  let valueTwo;
  let intersectionList = [];

  while (indexOne < nums1.length && indexTwo < nums2.length) {
    // if the previous pair is equal,
    // and those pair values map to the next iteration, advance
    // alternatively the storage solution could check for the existence of this element
    if (
      valueOne !== undefined &&
      valueTwo !== undefined &&
      valueOne === valueTwo &&
      nums1[indexOne] === valueOne &&
      nums2[indexTwo] === valueTwo
    ) {
      indexOne++
      indexTwo++
      continue;
    }


    valueOne = nums1[indexOne];
    valueTwo = nums2[indexTwo];

    if (valueOne === valueTwo) {
      intersectionList.push(valueOne);
      indexOne++;
      indexTwo++;
    } else if (valueOne < valueTwo) {
      indexOne++;
    } else {
      indexTwo++;
    }
  }

  return intersectionList;
}

export default intersection;
