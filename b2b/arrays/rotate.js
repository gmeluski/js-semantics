const rotate = (matrix) => {
  swapPerimeter(matrix, 0, matrix.length - 1);
  return matrix;
}

const swapPerimeter = (matrix, originBoundary, terminalBoundary) => {
  const hardBoundary = terminalBoundary - originBoundary;
  if (hardBoundary <= 0) {
    return;
  }

  // start at the edges
  let pointerA = originBoundary;
  let pointerC = terminalBoundary;

  while (pointerA < terminalBoundary) {
    const temp = matrix[pointerA][terminalBoundary];
    matrix[pointerA][terminalBoundary] = matrix[originBoundary][pointerA];
    matrix[originBoundary][pointerA] = matrix[pointerC][originBoundary];
    matrix[pointerC][originBoundary] = matrix[terminalBoundary][pointerC]
    matrix[terminalBoundary][pointerC] = temp;

    pointerA++;
    pointerC--;
  }

  const newOrigin = originBoundary + 1;
  const newTerminal = terminalBoundary - 1;
  swapPerimeter(matrix, newOrigin, newTerminal)
  return;
}


export default rotate;
