import rotate from '../rotate';

describe('rotating the matrix', () => {
  it('works in a 4x4', () => {
    const input = [
      [ 1,  2,  3, 4],
      [ 5,  6,  7, 8],
      [ 9, 10, 11, 12],
      [13, 14, 15, 16]
    ];

    const output = [
     [13,  9, 5, 1],
     [14, 10, 6, 2],
     [15, 11, 7, 3],
     [16, 12, 8, 4]
    ];
    expect(rotate(input)).toEqual(output);
  });

  it('works on a 2x2', () => {
    const input = [
      [10, 20],
      [30, 40]
    ];

    const output = [
     [30, 10],
     [40, 20]
    ];
  })
})
