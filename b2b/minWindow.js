const minWindow = (searchString, target) => {
  let bestLength = Number.MAX_VALUE;
  let bestMatch = '';

  for (var left = 0; left < searchString.length; left++) {
    for (var right = left; right < searchString.length; right++) {
      // create a larger and larger window each time
      const currentWindow = searchString.substring(left, right + 1);
      const foundAll = containsAllCharacters(currentWindow, target);
      if (foundAll && currentWindow.length < bestLength) {
        bestMatch = currentWindow;
        bestLength = currentWindow.length;
      }
    }
  }
  return bestMatch;
}

const containsAllCharacters = (searchString, target) => {
  const storage = new Map();

  for (var i = 0; i < target.length; i++) {
    if (storage.has(target[i])) {
      let count = storage.get(target[i]);
      storage.set(target[i], count + 1);
    } else {
      storage.set(target[i], 1);
    }
  }

  for (var i = 0; i < searchString.length; i++) {
    var currentCharacter = searchString[i];
    if (storage.has(currentCharacter)) {
      const currentCount = storage.get(currentCharacter)

      if (currentCount === 1) {
        storage.delete(currentCharacter);
      } else {
        storage.set(currentCharacter, currentCount - 1);
      }
    }
  }

  return storage.size === 0;
}

export default minWindow;
