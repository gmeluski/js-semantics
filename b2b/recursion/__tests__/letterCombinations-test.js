import letterCombinations from '../letterCombinations';

describe('letter combination cases', () => {
  it('works with a simple case', () => {
    const output = ["gd","ge","gf","hd","he","hf","id","ie","if"]
    expect(letterCombinations("43")).toEqual(output)
  });

  it('works with a complex case', () => {
    const input = "239";
    const output = [
      "adw","adx","ady","adz",
      "aew","aex","aey","aez",
      "afw","afx","afy","afz",
      "bdw","bdx","bdy","bdz",
      "bew","bex","bey","bez",
      "bfw","bfx","bfy","bfz",
      "cdw","cdx","cdy","cdz",
      "cew","cex","cey","cez",
      "cfw","cfx","cfy","cfz"
    ];

    expect(letterCombinations(input)).toEqual(output)
  });
});
