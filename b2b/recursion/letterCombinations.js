const lookup = {
  "2": ["a", "b", "c"],
  "3": ["d", "e", "f"],
  "4": ["g", "h", "i"],
  "5": ["j", "k", "l"],
  "6": ["m", "n", "o"],
  "7": ["p", "q", "r", "s"],
  "8": ["t", "u", "v"],
  "9": ["w", "x", "y", "z"],
}

const letterCombinations = (digits) => {
  const answers = [];
  detector(0, '', digits, answers);
  return answers;
}

const detector = (currentDigit, currentBuild, digits, answers) => {
  if (currentDigit === digits.length) {
    answers.push(currentBuild);
    return;
  }

  const letters = lookup[digits[currentDigit]];
  letters.forEach((letter) => {
    const nextDigit = currentDigit + 1;
    const amendedBuild = currentBuild.concat(letter);
    detector(nextDigit, amendedBuild, digits, answers);
  });
}

export default letterCombinations;
