const powerOfTwo = (input) => {
  let testNumber = input;
  let currentMod = 0;

  while (testNumber > 1 && currentMod === 0) {
    currentMod = testNumber % 2;
    testNumber = testNumber / 2;
  }

  return testNumber === 1;
}

export default powerOfTwo;
