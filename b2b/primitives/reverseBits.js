const reverseBits = (input) => {
  const bitRepresentation = (input >>> 0).toString(2);
  const bitList = bitRepresentation.split('');

  let leftPointer = 0;
  let rightPointer = bitList.length - 1;
  let output = bitList.slice();;

  while (leftPointer <= rightPointer) {
    let cupHolder = output[leftPointer];
    output[leftPointer] = output[rightPointer];
    output[rightPointer] = cupHolder
    leftPointer++;
    rightPointer--;
  }

  return parseInt(output.join(''), 2);
}

export default reverseBits;
