const convertToMinutes = (time) => {
  const timeTuple = time.split(':');
  const minutes = parseInt(timeTuple[0], 10) * 60 + parseInt(timeTuple[1], 10)

  return minutes;
}

const timeDifference = (times) => {
  let previousTime = -1;
  let bestTime = Infinity;
  let firstTime = -1;
  const minutesPerDay = 24 * 60;
  let allTimes = Array.from(Array(minutesPerDay)).map(() => false);

  const inMinutes = times.map(convertToMinutes);
  for (const time of inMinutes) {
    if (allTimes[time]) {
      bestTime = 0;
      break;
    }

    allTimes[time] = true
  };

  if (bestTime === 0) {
    return 0;
  }

  allTimes.forEach((seen, currentTime) => {
    if (seen) {
      if (previousTime !== -1) {
        const timeSpan = currentTime - previousTime;
        if (timeSpan < bestTime) {
          bestTime = timeSpan;
        }
      } else {
        firstTime = currentTime;
      }

      previousTime = currentTime;
    }
  })

  const loopTime = firstTime - 0 + minutesPerDay - previousTime;
  if (loopTime < bestTime) {
    bestTime = loopTime;
  }

  return bestTime;
}

export default timeDifference
