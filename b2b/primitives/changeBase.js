const overflowLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

const getOverflowLetter = (position) => {
  return overflowLetters[position - 10];
}

const convertDecimalToPositionedValue = (decimalNumber) => {
  return (decimalNumber < 10) ? decimalNumber : getOverflowLetter(decimalNumber);
}

const convertPositionedValueToDecimal = (value) => (
  (value < 10) ? value : overflowLetters.indexOf(value) + 10
)

const getMaxPower = (number, base) => {
  let result = Infinity;
  let power = -1;

  while (result >= 1) {
    power++;

    const poweredDenominator = Math.pow(base, power);
    result = number / poweredDenominator;
  }

  return power - 1;
}

const createNewBasedNumber = (currentPower, oldBased, toBase) => {
  let newBased = [];
  let toTally = oldBased;

  while (currentPower >= 0) {
    const denominator = Math.pow(toBase, currentPower);
    const toPush = Math.floor(toTally / denominator);
    newBased.push(convertDecimalToPositionedValue(toPush));
    toTally = toTally % denominator;
    currentPower--;
  }
  return newBased;
}

const convertBaseStringToBaseTenNumber = (string, base) => {
  const iteree = string.split('').reverse();
  return iteree.reduce((sum, value, index) => {
    return sum += Math.pow(base, index) * convertPositionedValueToDecimal(value);
  }, 0)
}

/**
 * @param {string} numAsString
 * @param {number} b1
 * @param {number} b2
 * @return {string}
 */
const changeBase = (numAsString, b1, b2) => {
  // if b1 === b2 then just return the string
  // if b1 !== 10 then we need to move it to base 10

  const startBaseTen = (b1 ===10) ? numAsString : convertBaseStringToBaseTenNumber(numAsString, b1);

  if (b2 === 10) {
    return startBaseTen.toString();
  }

  return createNewBasedNumber(getMaxPower(startBaseTen, b2), startBaseTen, b2).join('');
}

export default changeBase;
