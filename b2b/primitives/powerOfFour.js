const checkBinaryPosition = (inDecimal) => {
  let progressor = inDecimal
  let remainder = 0;
  let position = 0;

  while (progressor >= 1) {
    remainder = progressor % 2;
    progressor = progressor / 2;
    position++;
  }

  return position % 2 === 1;

}

const powerOfFour = (input) => {
  const bitwiseCheck = input & input - 1;
  const isPowerOfTwo = bitwiseCheck === 0;
  if (isPowerOfTwo) {
    return checkBinaryPosition(input);
  }

  return false;
}

export default powerOfFour;
