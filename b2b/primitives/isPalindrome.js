const isPalindrome = (x) => {
  if (x < 0) {
    return false;
  }

  if (x < 10) {
    return true;
  }

  let integerTest = x;
  let modValue;
  let ordered = [];

  while (integerTest > 0) {
    modValue = integerTest % 10;
    integerTest = Math.floor(integerTest / 10);
    ordered.unshift(modValue);
  }

  let left = 0;
  let right = ordered.length - 1;


  while (left <= right) {
    if (ordered[left] !== ordered[right]) {
      return false;
    }

    left++;
    right--;
  }

  return true;
}


export default isPalindrome;
