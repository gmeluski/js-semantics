const sortArray_0_1_2_twoPass = (nums) => {
  let zero = 0;
  let one = 0;
  let two = 0;

  nums.forEach((number) => {
    if (number === 0) {
      zero++;
    }

    if (number === 1) {
      one++;
    }

    if (number === 2) {
      two++;
    }
  });


  const length = nums.length;
  let sorted = [];
  for (let i = 0; i < length; i++) {
    if (zero !== 0) {
      sorted.push(0);
      zero--;
    } else if (one !== 0) {
      sorted.push(1);
      one--;
    } else {
      sorted.push(2);
    }
  }

  return sorted;
}

const swap = (array, indexOne, indexTwo) => {
  const temp = array[indexOne];
  array[indexOne] = array[indexTwo];
  array[indexTwo] = temp;

  return array;
}

const sortArray_0_1_2 = (nums) => {
  let currentIndex = 0;
  let leftIndex = 0;
  let rightIndex = nums.length - 1;

  while (currentIndex <= rightIndex) {
    const currentValue = nums[currentIndex];
    if (currentValue === 1) {
      currentIndex++;
    } else if (currentValue === 2) {
      const rightValue = nums[rightIndex];
      if (rightValue !== 2) {
        nums = swap(nums, currentIndex, rightIndex);
      }

      rightIndex--
    } else if (currentValue === 0) {
      const leftValue = nums[leftIndex];
      if (leftValue === 0)  {
        currentIndex++;
      } else {
        nums = swap(nums, currentIndex, leftIndex);
      }

      leftIndex++;
    }
  }

  return nums;
}

export default sortArray_0_1_2;
