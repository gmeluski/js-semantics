import reverseBits from '../reverseBits';

describe('reverse bits', () => {
  it('reverses one as expected', () => {
    const input = 1;
    const output = 1;

    expect(reverseBits(input)).toEqual(output);
  });

  it('reverses a simple integer', () => {
    const input = 10;
    const output = 5;

    expect(reverseBits(input)).toEqual(output);
  });

  it('reverses a more complex integer', () => {
    const input = 9090;
    const output = 4209;

    expect(reverseBits(input)).toEqual(output);
  });
});
