import powerOfTwo from '../powerOfTwo';


describe('power of two', () => {
  it('works on a simple power of two', () => {
    const input = 4;
    expect(powerOfTwo(input)).toEqual(true);
  });

  it('fails on an even number that is not a power of two', () => {
    const input = 6;
    expect(powerOfTwo(input)).toEqual(false);
  });

  it('fails on an odd number that is not a power of two', () => {
    const input = 17;
    expect(powerOfTwo(input)).toEqual(false);
  });

})
