import timeDifference from '../minimumTimeDifference';

describe('minimum time difference', () => {
  it('passes a basic case', () => {
    const input = ["00:03", "23:59", "12:03"]
    const output = 4
    expect(timeDifference(input)).toEqual(output);
  });

  it('passes a zero case', () => {
    const input = ["00:00","23:59","00:00"];
    const output = 0;
    expect(timeDifference(input)).toEqual(output);
  });
});
