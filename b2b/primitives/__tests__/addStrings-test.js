import addStrings from '../addStrings';

describe('add strings', () => {
  it('works', () => {
    const s1 = "95";
    const s2 = "7";

    expect(addStrings(s1, s2)).toEqual("102");
  })

  it('deals with zeros ok', () => {
    const s1 = "0";
    const s2 = "0";

    expect(addStrings(s1, s2)).toEqual("0");
  })

  it('deals with large numbers', () => {

    const s1 = "87654321123456789";
    const s2 = "123321123321123321123321";

    expect(addStrings(s1, s2)).toEqual("123321210975444444580110");
  })
});
