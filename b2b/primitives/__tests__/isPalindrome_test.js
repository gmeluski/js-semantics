import isPalindrome from '../isPalindrome';


describe('is palindrome', () => {
  it('returns false on non palindromes', () => {
    const value = 9232;
    expect(isPalindrome(value)).toEqual(false);
  });
  it('returns true on palindromes', () => {
    const value = 12321;
    expect(isPalindrome(value)).toEqual(true);
  })

  it('returns true on palindromes with 0s in place', () => {
    const value = 909;
    expect(isPalindrome(value)).toEqual(true);
  })

  it('returns true on single digits', () => {
    const value = 1;
    expect(isPalindrome(value)).toEqual(true);
  })

  it('returns false on negative numbers', () => {
    const value = -11;
    expect(isPalindrome(value)).toEqual(false);
  })
});
