import changeBase from '../changeBase';

describe('change base', () => {
  it('converts base 10 to binary', () => {
    const number = "12"
    const startBase = 10
    const endBase = 2;

    expect(changeBase(number, startBase, endBase)).toEqual('1100')
  })

  it('converts base x to base 10', () => {
    const number = "123";
    const startBase = 4;
    const endBase = 10;

    expect(changeBase(number, startBase, endBase)).toEqual('27')
  })

  it('converts base x to base y ', () => {
    const number = "123";
    const startBase = 4;
    const endBase = 16;

    expect(changeBase(number, startBase, endBase)).toEqual('1B')
  })

  it('converts base y to base x', () => {
    const number = '1B';
    const startBase = 16;
    const endBase = 4;

    expect(changeBase(number, startBase, endBase)).toEqual('123')
  });

  it('works with another case', () => {
    const number = "3456217"
    const startBase = 2
    const endBase = 9

    expect(changeBase(number, startBase, endBase)).toEqual('566')
  })
})
