import sortZeroOneTwo from '../sortZeroOneTwo';

describe('sort zero one two', () => {
  it('sorts short', () => {
    const input = [1, 2, 0, 2];
    const output = [0, 1, 2, 2]

    expect(sortZeroOneTwo(input)).toEqual(output);
  });

  it('sorts mixed', () => {
    const input = [0,1,2,1,2,2,2,1,0];
    const output = [0,0,1,1,1,2,2,2,2];

    expect(sortZeroOneTwo(input)).toEqual(output);
  })

  it('sorts a fully reversed array', () => {
    const input = [2,2,1,1,1,0,0,0,0];
    const output = [0,0,0,0,1,1,1,2,2];

    expect(sortZeroOneTwo(input)).toEqual(output);
  })
});
