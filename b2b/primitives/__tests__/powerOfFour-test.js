import powerOfFour from '../powerOfFour';
describe('testing', () => {
  it('returns false for an odd number', () => {
    expect(powerOfFour(5)).toEqual(false)
  });


  it('returns true for an even number which is a power of four', () => {
    expect(powerOfFour(16)).toEqual(true)
  });

  it('returns false for an even number which is not a power of four', () => {
    expect(powerOfFour(8)).toEqual(false)
  });
});
