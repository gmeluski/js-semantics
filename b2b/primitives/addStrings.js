const addStrings = (stringOne, stringTwo) => {
  let stringOnePointer = stringOne.length - 1;
  let stringTwoPointer = stringTwo.length - 1;
  let resultAsString = '';
  let carryThe = 0;

  while (stringOnePointer >= 0 || stringTwoPointer >= 0 || carryThe > 0) {
    let integerOne;
    let integerTwo;

    if (stringOnePointer >= 0) {
      integerOne = parseInt(stringOne[stringOnePointer], 10);
      stringOnePointer--;
    } else {
      integerOne = 0;
    }

    if (stringTwoPointer >= 0) {
      integerTwo = parseInt(stringTwo[stringTwoPointer], 10);
      stringTwoPointer--;
    } else {
      integerTwo = 0
    }

    const columnSum = integerOne + integerTwo + carryThe;
    const columnInteger = columnSum % 10;
    resultAsString = columnInteger + '' + resultAsString;

    carryThe = Math.floor(columnSum / 10);
  }

  return resultAsString;
}


const addStringsOld = (stringOne, stringTwo) => {
  const integerOne = convertString(stringOne);
  const integerTwo = convertString(stringTwo);

  let wholeInteger = integerOne + integerTwo;
  if (wholeInteger === 0) {
    return '0';
  }

  let wholeString = "";

  while (wholeInteger >= 1) {
    const remainder = wholeInteger % 10;
    wholeString = remainder.toString() + wholeString;
    wholeInteger = Math.floor(wholeInteger / 10);
  }

  return wholeString;
}

const convertString = (numberRepresentation) => {
  const ceiling = numberRepresentation.length;
  const zeroCharCode = "0".charCodeAt();
  let converted = 0;

  for (var i = 0; i < ceiling; i++) {
    const singleNumberRepresentation = numberRepresentation[i];
    const singleNumber = singleNumberRepresentation.charCodeAt() - zeroCharCode;
    const power = ceiling - (i + 1);
    converted = converted + (singleNumber * Math.pow(10, power));
  }

  return converted;
}

export default addStrings;
