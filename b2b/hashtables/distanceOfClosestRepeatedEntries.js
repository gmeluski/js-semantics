const distanceOfClosestRepeatedEntries = (sentence) => {
  let lastFound = {};
  let best = Number.POSITIVE_INFINITY;

  sentence.forEach((word, index) => {
    const lookup = lastFound[word];
    if (lookup !== undefined) {
      const distance = index - lookup;
      if (distance < best) {
        best = distance;
      }
    }

    lastFound[word] = index;
  });

  return best === Number.POSITIVE_INFINITY ? -1 : best;

}

export default distanceOfClosestRepeatedEntries;
