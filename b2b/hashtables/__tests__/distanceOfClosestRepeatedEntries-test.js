import distanceOfClosestRepeatedEntries from '../distanceOfClosestRepeatedEntries';

describe('stop the madness', () => {
  it('works for a basic sentence', () => {
    const sentence = [
      "This",
      "is",
      "a",
      "sentence",
      "with",
      "is",
      "repeated",
      "then",
      "repeated"
    ]

    expect(distanceOfClosestRepeatedEntries(sentence)).toEqual(2);

  })


  it('works when there are no repeats', () => {
    const sentence = [
      "This",
      "is",
      "a"
    ];

    expect(distanceOfClosestRepeatedEntries(sentence)).toEqual(-1);
  })

  it.only('works with multiple words', () => {
    const sentence = ["academy","awards","are","brought","to","you","by","other","academy","award","winners"]
    expect(distanceOfClosestRepeatedEntries(sentence)).toEqual(8);
  })
});
