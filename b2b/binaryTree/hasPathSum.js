
/**
 * @param {TreeNode} node
 * @param {number} targetSum
 * @return {boolean}
 */

const hasPathSum = (node, targetSum) => {
  if (!node) {
    return false;
  }

  if (!node.left && !node.right) {
    return (targetSum - node.val) === 0;
  }

  const newTarget = targetSum - node.val
  const isLeftValid = hasPathSum(node.left, newTarget)
  const isRightValid = hasPathSum(node.right, newTarget)

  return isLeftValid || isRightValid;
}

export default hasPathSum;
