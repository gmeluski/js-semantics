const isValidBST = (root) => {
  return isInRange(root, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER);
}

const isInRange = (node, minimum, maximum) => {
  if (!node.left && !node.right) {
    return minimum < node.value && node.value < maximum ;
  }

  const leftValue = isInRange(node.left, minimum, node.value);
  const rightValue = isInRange(node.right, node.value, maximum);

  return leftValue && rightValue;
}

const isValidBSTOld = (root) => {
  if (!root.left && !root.right) {
    return true;
  }

  let isLeftBalanced;
  if (root.left) {
    const maxLeftValue = findMaxValue(root.left);
    isLeftBalanced = maxLeftValue < root.value;
  } else {
    isLeftBalanced = true;
  }

  let isRightBalanced;
  if (root.right) {
    const minRightValue = findMinValue(root.right);
    isRightBalanced = minRightValue > root.value;
  } else {
    isRightBalanced = true;
  }

  return  isLeftBalanced && isRightBalanced;
}


const findMaxValue = (node) => {
  if (!node.left && !node.right) {
    return node.value;
  }
}

const findMinValue = (node) => {
  if (!node.left && !node.right) {
    return node.value;
  }
}

export default isValidBST;
