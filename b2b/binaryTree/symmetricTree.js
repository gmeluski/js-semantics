
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
const isSymmetric = (root) => {
   if (root === null) {
    return true;
  }

  return symmetricTree(root.left, root.right);
}

const symmetricTree = (leftTree, rightTree) => {
  if (leftTree === null && rightTree === null) {
    return true;
  }

  if (leftTree !== null && rightTree !== null) {
    const oppositeTrees = symmetricTree(leftTree.right, rightTree.left);
    const sameTrees = symmetricTree(leftTree.left, rightTree.right);

    return (leftTree.val === rightTree.val) && oppositeTrees && sameTrees;
  }

  return false;
}


export default isSymmetric;
