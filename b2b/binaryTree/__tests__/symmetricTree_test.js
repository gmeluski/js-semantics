import symmetricTree from '../symmetricTree.js'
import TreeNode from '../TreeNode';


describe('symmetric tree', () => {
  it('returns true when symmetric', () => {
    const left = new TreeNode(1);
    const right = new TreeNode(1);
    const root = new TreeNode(2, left, right);

    const isSymmetric = symmetricTree(root)
    expect(isSymmetric).toEqual(true);

  });

  it('returns false otherwise', () => {
    const leafOne = new TreeNode(1)
    const leafTwo = new TreeNode(2)
    const balancedChild = new TreeNode(2, leafOne, leafTwo);

    const leafThree = new TreeNode(2)
    const unbalancedChild = new TreeNode(2, leafThree);

    const root = new TreeNode(4, balancedChild, unbalancedChild);

    const isSymmetric = symmetricTree(root)
    expect(isSymmetric).toEqual(false);
  });

  it('returns tree when the root node has no children', () => {
    const isSymmetric = symmetricTree(null)
    expect(isSymmetric).toEqual(true);
  });

});
