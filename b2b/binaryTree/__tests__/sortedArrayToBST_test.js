import sortedArrayToBST from '../sortedArrayToBST';

describe('sorted array', () => {
  xit('returns the simple case', () => {
    const input = [0, 1, 2];
    const output = [1, 0, 2];
    const result = sortedArrayToBST(input);
    expect(result).toEqual(output);
  });

  xit('returns true when asymmetric', () => {
    const input = [1, 2, 3, 4, 5, 6, 7];
    const output = [4, 2, 6, 1, 3, 5, 7];
    const result = sortedArrayToBST(input);

    console.log('############', result);
    expect(result).toEqual(output);
  });

  xit('returns true when symmetric', () => {
    const input = [-3, 1, 2, 3, 5, 6, 8, 11];
    const output = [5, 2, 8, 1, 3, 6, 11, -3]
    const x = sortedArrayToBST(input);
  });
});
