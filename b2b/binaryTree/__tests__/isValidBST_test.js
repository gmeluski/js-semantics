import isValidBST from '../isValidBST'
import TreeNode from '../TreeNode';

describe('isValidBST', () => {
  it('verifies the base case', () => {
    const tree = [-1];
    const nodes = tree.map(value => new TreeNode(value));

    expect(isValidBST(nodes[0])).toEqual(true);
  });

  it('verifies a simple tree', () => {
    const tree = [-1, -3, 0];
    const nodes = tree.map(value => new TreeNode(value));
    const level = nodes.slice(1);
    const root = nodes.slice(0, 1)[0];

    root.left = level[0];
    root.right = level[1];

    expect(isValidBST(root)).toEqual(true);
  })

  it('fails a simple tree', () => {
    const tree = [2,4, null];
    const nodes = tree.map(value => new TreeNode(value));
    const level = nodes.slice(1);
    const root = nodes.slice(0, 1)[0];

    root.left = level[0];
    root.right = level[1];

    expect(isValidBST(root)).toEqual(false);
  })

  it('verifies a more complex tree', () => {
    const tree = [2,1,4,null,null,3,5];
    const nodes = tree.map(value => new TreeNode(value));


    nodes[2].right = nodes[6];
    nodes[2].left = nodes[5];
    nodes[0].right = nodes[2];
    nodes[0].left = nodes[1];
    const root = nodes[0];
    expect(isValidBST(root)).toEqual(true);
  })

  it('fails a complex tree', () => {
    const tree = [10,2,7,null,null,3,12];
    const nodes = tree.map(value => new TreeNode(value));

    for (let i = nodes.length - 1; i > 0; i--) {
      const test = i - 1;
      const parentIndex = Math.floor(test / 2);
      const side = (test % 2 == 0) ? 'left' : 'right';
      const nodeToAssign = nodes[i];

      nodes[parentIndex][`${side}`] = nodes[i];
    }

    expect(isValidBST(nodes[0])).toEqual(false);
  })

});
