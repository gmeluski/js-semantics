import hasPathSum from '../hasPathSum'
import TreeNode from '../TreeNode';

describe('hasPathSum', () => {
  const target = 5;

  it('works with a small unbalanced tree', () => {
    const values = [1, null, 4];
    const nodes = values.map(value => new TreeNode(value));

    const root = nodes.slice(0, 1)[0];
    root.right = nodes[2];

    const result = hasPathSum(root, target)
    expect(result).toEqual(true);
  });

  it('works with a medium unbalanced tree', () => {
    const values = [1, null, 2, null, null, 2, 5];
    const nodes = values.map(value => new TreeNode(value));

    const thirdLevel = nodes.slice(3, 7);
    const secondLevelNode = nodes.slice(1, 3)[1];
    secondLevelNode.left = thirdLevel[2];
    secondLevelNode.right = thirdLevel[3];

    const root = nodes.slice(0, 1)[0];
    root.right = secondLevelNode;

    const result = hasPathSum(root, target)
    expect(result).toEqual(true);
  });
 });
