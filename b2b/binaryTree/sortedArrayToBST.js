import TreeNode from '../TreeNode';

const sortedArrayToBST = (nums) => {
  return build(nums, 0, nums.length);
}

const build = (nums, left, right) => {
  if (left >= right) {
    return null;
  }

  const midpoint = left + ((right - left) / 2)

}

const sortedArrayToBSTOld = (nums) => {
  if (nums.length === 0) {
    return [];
  }

  if (nums.length === 1) {
    console.log('root case', nums);
    return nums;
  }

  const midpointIndex = Math.floor(nums.length / 2);

  console.log('midpoint index', midpointIndex)
  const midpoint = [...nums].slice(midpointIndex, midpointIndex + 1)
  console.log('midpoint !!!!!!!!!!!!!!!1', midpoint)

  console.log('executing left')
  const leftSide = nums.slice().splice(0, midpointIndex);
  const leftResult = sortedArrayToBST(leftSide);
  console.log('################', leftResult)

  console.log('executing right')
  const rightSide = nums.slice().splice(midpointIndex + 1);
  const rightResult = sortedArrayToBST(rightSide);
  console.log('$$$$$$$$$$$$$$$4', rightResult);

  console.log(`%%%%%%%%%% ${midpoint},  ${leftResult},  ${rightResult}`)
  return [...midpoint];
}


const getLevelsByLength = (length) => {
  if (length === 0) {
    return 0;
  }

  const logger = Math.log2(length);

  if (logger % 1 === 0) {
    return logger + 1;
  }

  return Math.ceil(logger);
}

export default sortedArrayToBST;
